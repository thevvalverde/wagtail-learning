from urllib.parse import urlparse, urlunparse

from django.http import QueryDict
from django.template import Library

from ..models import BlogCategory as Category
from ..models import Tag

register = Library()


@register.inclusion_tag("blog/components/categories_list.html", takes_context=True)
def categories_list(context):
    categories = Category.objects.all()
    return {
        "request": context["request"],
        "blog_page": context["blog_page"],
        "categories": categories,
    }


@register.inclusion_tag("blog/components/tags_list.html", takes_context=True)
def tags_list(context):
    tags = Tag.objects.all()
    return {
        "request": context["request"],
        "blog_page": context["blog_page"],
        "tags": tags,
    }


@register.inclusion_tag("blog/components/post_categories_list.html", takes_context=True)
def post_categories_list(context):
    page = context["page"]
    post_categories = page.categories.all()
    return {
        "request": context["request"],
        "blog_page": context["blog_page"],
        "post_categories": post_categories,
    }


@register.inclusion_tag("blog/components/post_tags_list.html", takes_context=True)
def post_tags_list(context):
    page = context["page"]
    post_tags = page.tags.all()
    return {
        "request": context["request"],
        "blog_page": context["blog_page"],
        "post_tags": post_tags,
    }


@register.simple_tag
def url_replace(request, **kwargs):
    """
    Replace or add querystring

    To replace the page field in the URL
    {% url_replace request page=page_num %}

    :param request: current request
    :param kwargs: replacer
    :return: urlunparse
    """
    (scheme, netloc, path, params, query, fragment) = urlparse(request.get_full_path())
    query_dict = QueryDict(query, mutable=True)
    for key, value in kwargs.items():
        query_dict[key] = value
    query = query_dict.urlencode()
    return urlunparse((scheme, netloc, path, params, query, fragment))


@register.simple_tag
def post_page_date_slug_url(post_page, blog_page):
    post_date = post_page.post_date
    url = blog_page.full_url + blog_page.reverse_subpage(
        "post_by_date_slug",
        args=(
            post_date.year,
            f"{post_date.month:02}",
            f"{post_date.day:02}",
            post_page.slug,
        ),
    )
    return url
